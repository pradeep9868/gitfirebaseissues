package com.rx.myapplication.viewmodel;


import androidx.databinding.BaseObservable;

/**
 * Created by Pradeep Kumar on 2/13/2019.
 */
public class CommentsActivityViewModel extends BaseObservable {

    /** CLASS VARIABLES ________________________________________________________________________ **/

    // VISIBILITY VARIABLES
    private boolean errorTextVisible = false;
    private boolean progressBarVisible = false;

    /** VIEW MODEL METHODS _____________________________________________________________________ **/

    public boolean isErrorTextVisible() {
        return errorTextVisible;
    }

    public void setErrorTextVisible(boolean errorTextVisible) {
        this.errorTextVisible = errorTextVisible;
        this.notifyChange();
    }

    public boolean isProgressBarVisible() {
        return progressBarVisible;
    }

    public void setProgressBarVisible(boolean progressBarVisible) {
        this.progressBarVisible = progressBarVisible;
        this.notifyChange();
    }
}
