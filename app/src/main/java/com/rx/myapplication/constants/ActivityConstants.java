package com.rx.myapplication.constants;

/**
 * Created by Pradeep Kumar on 2/12/2019.
 */
public class ActivityConstants {

    /** BUNDLE CONSTANTS _______________________________________________________________________ **/

    public static final String GIT_ISSUE_CONTENT = "com.rx.myapplication.activities.MainActivity.GIT_ISSUE_CONTENT";
}
