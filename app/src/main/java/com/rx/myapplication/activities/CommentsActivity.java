package com.rx.myapplication.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rx.myapplication.R;
import com.rx.myapplication.constants.ActivityConstants;
import com.rx.myapplication.constants.GitConstants;
import com.rx.myapplication.databinding.ActivityCommentsBinding;
import com.rx.myapplication.interfaces.RetrofitInterface;
import com.rx.myapplication.models.Comment;
import com.rx.myapplication.ui.CommentsAdapter;
import com.rx.myapplication.viewmodel.CommentsActivityViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pradeep Kumar on 2/12/2019.
 */
public class CommentsActivity extends AppCompatActivity {

    /** CLASS VARIABLES ________________________________________________________________________ **/

    // BINDING / VIEWMODEL VARIABLES
    private ActivityCommentsBinding commentsActivityBinding;
    private CommentsActivityViewModel commentsActivityViewModel;

    // CONSTANTS VARIABLES
    private static final int NUMBER_OF_PREFETCH_ITEMS = 3;

    // LIST VARIABLES
    private List<Comment> commentsListResult;
    private int currentIssue = 0;

    // LOGGING VARIABLES
    private static final String LOG_TAG = CommentsActivity.class.getSimpleName();

    /** ACTIVITY LIFECYCLE METHODS _____________________________________________________________ **/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBinding();
        initToolbar();
        initExtras();
    }

    /** ACTIVITY OVERRIDE METHODS ______________________________________________________________ **/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** LAYOUT METHODS _________________________________________________________________________ **/

    private void initBinding() {
        commentsActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_comments);
        commentsActivityViewModel = new CommentsActivityViewModel();
        commentsActivityBinding.setViewModel(commentsActivityViewModel);
    }

    private void initToolbar() {
        commentsActivityBinding.gitCommentsActivityToolbar.setTitle(getResources().getString(R.string.comments_title));
        setSupportActionBar(commentsActivityBinding.gitCommentsActivityToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initExtras() {
        if (getIntent().getExtras() != null) {
            currentIssue = getIntent().getIntExtra(ActivityConstants.GIT_ISSUE_CONTENT, 0);
        }

        if (currentIssue != 0) {
            commentsActivityViewModel.setProgressBarVisible(true);
            retrieveComments();
        } else {
            commentsActivityViewModel.setErrorTextVisible(true);
        }
    }

    private void updateView(boolean issuesReceived) {

        commentsActivityViewModel.setProgressBarVisible(false);

        if (issuesReceived) {
            initRecyclerView();
            setRecyclerList(commentsListResult);
        }
    }

    /** RECYCLERVIEW METHODS ___________________________________________________________________ **/

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setItemPrefetchEnabled(true);
        layoutManager.setInitialPrefetchItemCount(NUMBER_OF_PREFETCH_ITEMS);
        commentsActivityBinding.gitCommentsActivityRecyclerView.setLayoutManager(layoutManager);
    }

    private void setRecyclerList(List<Comment> commentList){
        CommentsAdapter recyclerAdapter = new CommentsAdapter(commentList, this);
        recyclerAdapter.setHasStableIds(true);
        commentsActivityBinding.gitCommentsActivityRecyclerView.setAdapter(recyclerAdapter);
        commentsActivityBinding.gitCommentsActivityRecyclerView.setHasFixedSize(true);
        commentsActivityBinding.gitCommentsActivityRecyclerView.setDrawingCacheEnabled(true);
        commentsActivityBinding.gitCommentsActivityRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    /** RETROFIT METHODS _______________________________________________________________________ **/

    private void retrieveComments() {

        Retrofit retrofitAdapter = new Retrofit.Builder()
                .baseUrl(GitConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        commentsListResult = new ArrayList<>();

        RetrofitInterface apiRequest = retrofitAdapter.create(RetrofitInterface.class);
        Observable<List<Comment>> call = apiRequest.getComments(GitConstants.GIT_USER, GitConstants.GIT_REPO,
                currentIssue, GitConstants.GIT_SORT_UPDATED);
        call.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Comment>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Comment> comments) {
                        commentsListResult = comments;
                        if (comments != null) {
                            updateView(true);
                            Log.d(LOG_TAG, "retrieveComments(): Retrieved issues in " + GitConstants.GIT_REPO + " from GitHub.");
                        } else {
                            Log.e(LOG_TAG, "retrieveComments(): ERROR: The retrieved comments were null.");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(LOG_TAG, "retrieveComments(): ERROR: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
}
}