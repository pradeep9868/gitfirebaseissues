package com.rx.myapplication.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.rx.myapplication.R;
import com.rx.myapplication.constants.GitConstants;
import com.rx.myapplication.databinding.ActivityMainBinding;
import com.rx.myapplication.interfaces.RetrofitInterface;
import com.rx.myapplication.models.Issue;
import com.rx.myapplication.ui.IssuesAdapter;
import com.rx.myapplication.viewmodel.MainViewModel;

import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pradeep Kumar on 2/12/2019.
 */

public class MainActivity extends AppCompatActivity implements MainViewModel.MainViewModelListener {

    /** CLASS VARIABLES ________________________________________________________________________ **/

    // BINDING / VIEWMODEL VARIABLES
    private ActivityMainBinding mainActivityBinding;
    private MainViewModel mainActivityViewModel;

    // CONSTANTS VARIABLES
    private static final int NUMBER_OF_PREFETCH_ITEMS = 4;

    // LIST VARIABLES
    private List<Issue> issuesListResult;

    // LOGGING VARIABLES
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    /** ACTIVITY LIFECYCLE METHODS _____________________________________________________________ **/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBinding();
        initToolbar();
        initText();
        mainActivityViewModel.load_data();
    }

    /** LAYOUT METHODS _________________________________________________________________________ **/

    private void initBinding() {
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainActivityViewModel = new MainViewModel();
        mainActivityViewModel.setMainViewModelListener(this);
        mainActivityBinding.setViewModel(mainActivityViewModel);
    }

    private void initToolbar() {
        setSupportActionBar(mainActivityBinding.gitMainActivityToolbar);
    }

    private void initText() {
        mainActivityBinding.repoOpenIssuesText.setShadowLayer(4, 2, 2, Color.BLACK);
        mainActivityBinding.repoOpenIssuesText.setShadowLayer(4, 2, 2, Color.BLACK);
    }

    private void displaySnackbar() {
        Snackbar.make(mainActivityBinding.gitMainActivityLayout, String.format(getResources().getString(R.string.open_issues_snackbar_message), issuesListResult.size()), Snackbar.LENGTH_LONG).show();
    }

    private void updateView(boolean issuesReceived) {

        mainActivityViewModel.setMainProgressBarVisibility(false);

        if (issuesReceived) {
            initRecyclerView();
            setRecyclerList(issuesListResult);
            mainActivityViewModel.setOpenIssuesValueText(" " + issuesListResult.size());
            mainActivityViewModel.setOpenIssueContainerVisibility(true);
            displaySnackbar();
        }
    }

    /** RECYCLERVIEW METHODS ___________________________________________________________________ **/

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setItemPrefetchEnabled(true);
        layoutManager.setInitialPrefetchItemCount(NUMBER_OF_PREFETCH_ITEMS);
        mainActivityBinding.gitMainActivityRecyclerView.setLayoutManager(layoutManager);
    }

    private void setRecyclerList(List<Issue> issueList){
        IssuesAdapter recyclerAdapter = new IssuesAdapter(issueList, this);
        recyclerAdapter.setHasStableIds(true);
        mainActivityBinding.gitMainActivityRecyclerView.setAdapter(recyclerAdapter);
        mainActivityBinding.gitMainActivityRecyclerView.setHasFixedSize(true);
        mainActivityBinding.gitMainActivityRecyclerView.setDrawingCacheEnabled(true);
        mainActivityBinding.gitMainActivityRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    /** RETROFIT METHODS _______________________________________________________________________ **/

    private void retrieveIssues() {
        long cacheSize = 5 * 1024 * 1024;
        File httpCacheDirecotory = new File(getCacheDir(), "retro_cache");
        Cache myCache = new Cache(httpCacheDirecotory, cacheSize);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().cache(myCache).addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                //Code to check for 24 hour cache data
                Request request = chain.request();
                request = request.newBuilder().header("Cache-Control", "public, max-stale=" + 60 * 60 * 24).build();
                return chain.proceed(request);
            }
        }).build();

        Retrofit retrofitAdapter = new Retrofit.Builder()
                .baseUrl(GitConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(okHttpClient)
                .build();

        issuesListResult = new ArrayList<>();

        RetrofitInterface apiRequest = retrofitAdapter.create(RetrofitInterface.class);
        Observable<List<Issue>> call = apiRequest.getIssues(GitConstants.GIT_USER, GitConstants.GIT_REPO,
                GitConstants.GIT_SORT_UPDATED, GitConstants.GIT_STATE_OPEN, GitConstants.GIT_PAGE_ISSUE_LIMIT);

        call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Issue>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Issue> issues) {
                        issuesListResult = issues;
                        if (issues != null) {
                            updateView(true);
                            Log.d(LOG_TAG, "retrieveIssues(): Retrieved issues in " + GitConstants.GIT_REPO + " from GitHub.");
                        } else {
                            Log.e(LOG_TAG, "retrieveIssues(): ERROR: The retrieved issues were null.");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(LOG_TAG, "retrieveIssues(): ERROR: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    /** LISTENER METHODS _______________________________________________________________________ **/

    @Override
    public void onFabButtonClicked() {
        mainActivityViewModel.setMainProgressBarVisibility(true);
        retrieveIssues();
    }
}