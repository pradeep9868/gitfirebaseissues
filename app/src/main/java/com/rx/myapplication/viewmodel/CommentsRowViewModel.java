package com.rx.myapplication.viewmodel;

import androidx.databinding.BaseObservable;

/**
 * Created by Pradeep Kumar on 2/13/2019.
 */
public class CommentsRowViewModel extends BaseObservable {

    /** CLASS VARIABLES ________________________________________________________________________ **/

    // TEXT VARIABLES
    private String commentsNameText;
    private String commentsText;
    private String commentsTimeText;

    /** VIEW MODEL METHODS _____________________________________________________________________ **/

    public String getCommentsNameText() {
        return commentsNameText;
    }

    public String getCommentsText() {
        return commentsText;
    }

    public String getCommentsTimeText() {
        return commentsTimeText;
    }

    public void setCommentsNameText(String commentsNameText) {
        this.commentsNameText = commentsNameText;
    }

    public void setCommentsText(String commentsText) {
        this.commentsText = commentsText;
    }

    public void setCommentsTimeText(String commentsTimeText) {
        this.commentsTimeText = commentsTimeText;
    }
}