package com.rx.myapplication.constants;

/**
 * Created by Pradeep Kumar on 2/12/2019.
 */
public class GitConstants {

    /** GITHUB CONSTANTS _______________________________________________________________________ **/

    public final static String GIT_USER = "firebase";
    public final static String GIT_REPO = "firebase-ios-sdk";

    /** URL CONSTANTS __________________________________________________________________________ **/

    public final static String BASE_URL = "https://api.github.com";

    /** ISSUE CONSTANTS ________________________________________________________________________ **/

    public final static int GIT_PAGE_ISSUE_LIMIT = 100;
    public final static String GIT_STATE_OPEN = "open";
    public final static String GIT_STATE_CLOSED = "closed";
    public final static String GIT_STATE_ALL = "all";
    public final static String GIT_SORT_CREATED = "created";
    public final static String GIT_SORT_UPDATED = "updated";
    public final static String GIT_SORT_COMMENTS = "comments";
}
