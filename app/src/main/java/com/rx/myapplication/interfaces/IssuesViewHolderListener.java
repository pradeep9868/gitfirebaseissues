package com.rx.myapplication.interfaces;

import android.view.View;

/**
 * Created by Pradeep Kumar on 2/12/2019.
 */

public interface IssuesViewHolderListener {
    void onIssueClick(View view, int position);
}